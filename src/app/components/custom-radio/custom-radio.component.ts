import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-custom-radio',
  templateUrl: './custom-radio.component.html',
  styleUrls: ['./custom-radio.component.scss']
})
export class CustomRadioComponent implements OnInit {

  constructor() { }

  @Input() question: any;
  @Output() valueChange = new EventEmitter();
  @Output() validChange = new EventEmitter();

  customFormControl: any;
  matcher = new MyErrorStateMatcher();

  ngOnInit(): void {
    this.initFormControl();
  }

  onValueChange(e: any) {
    this.valueChange.emit(this.question.value);
    this.validChange.emit(this.customFormControl.errors);
  }

  initFormControl() {
    this.customFormControl = new FormControl('', [
      Validators.required
    ]);
  }
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}