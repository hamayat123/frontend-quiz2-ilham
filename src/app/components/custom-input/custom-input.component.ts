import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-custom-input',
  templateUrl: './custom-input.component.html',
  styleUrls: ['./custom-input.component.scss']
})
export class CustomInputComponent implements OnInit {

  constructor() { }

  @Input() question: any;
  @Output() valueChange = new EventEmitter();
  @Output() validChange = new EventEmitter();

  customFormControl: any;
  matcher = new MyErrorStateMatcher();

  ngOnInit(): void {
    this.initFormControl();
  }

  onValueChange(e: any) {
    this.valueChange.emit(this.question.value);
    this.validChange.emit(this.customFormControl.errors);
  }

  initFormControl() {
    if (this.question.type == 'email') {
      this.customFormControl = new FormControl('', [
        Validators.required,
        Validators.email
      ]);
    } else if (this.question.type == 'number') {
      this.customFormControl = new FormControl('', [
        Validators.required,
        Validators.min(1),
        Validators.max(200)
      ]);
    } else {
      this.customFormControl = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50)
      ]);
    }
  }

  get getPlaceholder() {
    if (this.question.type == 'email') {
      return 'Contoh: example@mail.com';
    } else if (this.question.type == 'number') {
      return 'Contoh: 5';
    } else {
      return 'Contoh: John Doe';
    }
  }
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}