import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-custom-textarea',
  templateUrl: './custom-textarea.component.html',
  styleUrls: ['./custom-textarea.component.scss']
})
export class CustomTextareaComponent implements OnInit {

  constructor() { }

  @Input() question: any;
  @Output() valueChange = new EventEmitter();
  @Output() validChange = new EventEmitter();

  customFormControl: any;
  matcher = new MyErrorStateMatcher();

  ngOnInit(): void {
    this.initFormControl();
  }

  onValueChange(e: any) {
    this.valueChange.emit(this.question.value);
    this.validChange.emit(this.customFormControl.errors);
  }

  initFormControl() {
    this.customFormControl = new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(250)
    ]);
  }

  get getPlaceholder() {
    return 'Contoh: Untuk hadiah';
  }
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}