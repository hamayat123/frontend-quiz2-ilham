import { Component, OnInit } from '@angular/core';
import questions from '../../_files/questions.json';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
})
export class QuestionComponent implements OnInit {
  isValid = false;
  isFinish = false;
  questionsList: {
    isActive: Boolean;
    question: String;
    type: String;
    value: any;
    options: any;
  }[] = questions;

  constructor() {}

  ngOnInit(): void {}

  onValueChange(i: any, newValue: any) {
    this.questionsList[i].value = newValue;
  }

  onValidChange(errors: any) {
    if (errors == null) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
  }

  next(i: number) {
    this.questionsList[i].isActive = false;
    this.questionsList[i + 1].isActive = true;
  }

  save() {
    this.isFinish = true;
  }
}
